import Vue from "vue";
import App from "./App.vue";
import component1 from "./components/component1";
import component2 from "./components/component2";
import VueRouter from "vue-router";

Vue.config.productionTip = false;
Vue.use(VueRouter);

const routes = [
  { path: "/first", component: component1 },
  { path: "/second", component: component2 }
];

const router = new VueRouter({
  routes // short for `routes: routes`
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
