import { shallowMount } from "@vue/test-utils";
import component1 from "@/components/component1.vue";

describe("component1.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(component1, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
